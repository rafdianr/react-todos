import axios from "axios";

export const loadTodos = () => async (dispatch) => {
  await axios
    .get("https://jsonplaceholder.typicode.com/posts")
    .then((res) => {
      dispatch({
        type: "LOAD_TODOS",
        payload: res.data,
      });
    })
    .catch((e) => console.log("error", { e }));
};

export const addTodo = (newData) => async (dispatch) => {
  await axios
    .post("https://jsonplaceholder.typicode.com/posts", newData)
    .then((res) => {
      dispatch({
        type: "ADD_TODO",
        payload: res.data,
      });
    })
    .catch((e) => console.log({ e }));
};

export const deleteTodo = (id) => async (dispatch) => {
  await axios
    .delete(`https://jsonplaceholder.typicode.com/posts/${id}`)
    .then((res) => {
      // filter new data
      dispatch({
        type: "DELETE_TODO",
        payload: id,
      });
    })
    .catch((e) => console.log({ e }));
};
