const inittialState = {
  data: [],
};

const todos = (state = inittialState, action) => {
  switch (action.type) {
    case "LOAD_TODOS":
      return {
        ...state,
        data: action.payload,
      };
    case "ADD_TODO":
      return {
        ...state,
        data: [action.payload, ...state.data],
      };
    case "DELETE_TODO":
      return {
        ...state,
        data: [...state.data.filter((item) => item.id !== action.payload)],
      };
    default:
      return state;
  }
};

export default todos;
