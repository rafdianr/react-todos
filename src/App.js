import "./App.css";
import TodoList from "./components/TodoList";
import configureStore from "./store/configure-store";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";

const { store, persistor } = configureStore();

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <div className="App">
          <TodoList />
        </div>
      </PersistGate>
    </Provider>
  );
};

export default App;
