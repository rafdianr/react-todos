import React, { useState } from "react";
import { useDispatch } from "react-redux";
import "../assets/styles/NewTodoForm.css";
import { addTodo } from "../store/actions/todos";

const NewTodoForm = ({ data }) => {
  const [inputValue, setInputValue] = useState("");
  const dispatch = useDispatch();

  const inputTodo = () => {
    console.log("value", inputValue);
    const newData = {
      userId: 2,
      id: data.length + 1,
      title: inputValue,
    };
    dispatch(addTodo(newData));
  };

  return (
    <div className="new-todo-form">
      <input
        className="new-todo-input"
        type="text"
        placeholder="Type your new todo here"
        value={inputValue}
        onChange={(e) => setInputValue(e.target.value)}
      />
      <button
        disabled={!inputValue}
        className="new-todo-button"
        onClick={inputTodo}
      >
        Create Todo
      </button>
    </div>
  );
};

export default NewTodoForm;
