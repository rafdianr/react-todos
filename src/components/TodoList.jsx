import React, { useEffect } from "react";
import NewTodoForm from "./NewTodoForm";
import TodoListItem from "./TodoListItem";
import "../assets/styles/TodoList.css";
import { useDispatch, useSelector } from "react-redux";
import { deleteTodo, loadTodos } from "../store/actions/todos";

const TodoList = () => {
  const todosData = useSelector((state) => state.todos.data);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loadTodos());
  }, [dispatch]);

  const delTodo = (id) => {
    dispatch(deleteTodo(id));
  };

  return (
    <div className="list-wrapper">
      <NewTodoForm data={todosData} />
      {todosData.map((todo, index) => (
        <TodoListItem todo={todo} key={index} delTodo={delTodo} />
      ))}
    </div>
  );
};

export default TodoList;
